# UNSW_MIT_studygroup


## Good resources for Py
* [CheckIO](checkio.com)


> It's an website that provides tons of quizzes for python beginer/advanced, which are (Of cause) sorted by difficulty.
> Hope you enjoy it.

* [IT ebook searching](https://github.com/MrAlex6204/Books/find/master)


## FAQ

* [How to join this project as member](https://docs.gitlab.com/ee/user/project/members/#request-access-to-a-project)

* How to Git?

> For GUI(Graphical User Interface), SourceTree is recommended: [Download](https://www.sourcetreeapp.com),  [Tutorial](https://confluence.atlassian.com/get-started-with-sourcetree)
>
> For learning Git: [Pro Git](https://git-scm.com/book/en/v2) has a good reputation. [Think like a Git](http://think-like-a-git.net/) is read and recommended by Jason.
